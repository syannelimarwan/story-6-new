from django.shortcuts import render, redirect
from .models import StatusBaru
from .forms import BikinForm

# Create your views here.
def index(request):
    if request.method == "POST":
        form = BikinForm(request.POST)
        if form.is_valid():
            status = form.save(commit=False)
            status.save()
            return redirect('index')
    else:
        form = BikinForm()
    status = StatusBaru.objects.all().order_by('-waktu')
    return render(request, 'page1.html', {'form' : form, 'status' : status})

