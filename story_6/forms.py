from django import forms
from .models import StatusBaru
from django.forms import widgets

class BikinForm(forms.ModelForm):

    class Meta():
        model = StatusBaru
        fields = {'status'}
        widgets = {
            'status' : forms.Textarea(attrs={'cols':10, 'rows': 5}),
        }

    def __init__(self, *args, **kwargs):
        super(BikinForm,self).__init__(*args, **kwargs)
        for field in self.Meta.fields:
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })
