from django.test import TestCase, LiveServerTestCase
from django.urls import resolve
from .models import StatusBaru
from .views import index
from .forms import BikinForm

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


# Create your tests here.
class story6UnitTest(TestCase):

    def test_landing_is_true(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'page1.html')
        self.assertContains(response,'Halo, apa kabar?',count=1,status_code=200,html = True)

    def test_landing_use_correct_views(self):
        target = resolve('/')
        self.assertEqual(target.func, index)

    def test_check_status(self):
        c = self.create_status()
        self.assertTrue(isinstance(c, StatusBaru))
        self.assertTrue(c.__str__(), c.status)
        counting_all_status = StatusBaru.objects.all().count()
        self.assertEqual(counting_all_status, 1)

    def create_status(self):
        new_status = StatusBaru.objects.create(status = "Coba Bikin Status")
        return new_status

    def test_models(self):
        s = self.create_status()

    def test_form(self):
        form_data = {
        'status' : 'ini adalah sebuah status',
        }
        form = BikinForm(data = form_data)
        self.assertTrue(form.is_valid())
        request = self.client.post('/', data = form_data)
        self.assertEqual(request.status_code, 302)

        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

class StorySixFunctionalTest(LiveServerTestCase):

    def setUp(self):
        # chrome
        super(StorySixFunctionalTest, self).setUp()
        chrome_options = Options()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        self.browser = webdriver.Chrome('./chromedriver',chrome_options = chrome_options)

    def tearDown(self):
        self.browser.quit()
        super(StorySixFunctionalTest, self).tearDown()

    def test_input_status(self):
        self.browser.get('http://127.0.0.1:8000')

        status = self.browser.find_element_by_id('id_status')
        submit = self.browser.find_element_by_id('submit')

        status.send_keys("Coba Coba")
        submit.send_keys(Keys.RETURN)


    



