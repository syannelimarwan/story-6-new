from django.db import models

# Create your models here.
class StatusBaru(models.Model):
    status = models.CharField(max_length = 300, default="")
    waktu = models.DateTimeField(auto_now_add=True, editable=False, null=False, blank=False)

    def __str__(self):
        return self.status
